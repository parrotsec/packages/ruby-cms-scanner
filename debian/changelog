ruby-cms-scanner (0.14.3-0parrot1) lory-updates; urgency=medium

  * Import new upstream release

 -- Dario "danterolle" Camonita <danterolle@parrotsec.org>  Mon, 20 Jan 2025 18:12:49 +0100

ruby-cms-scanner (0.13.7-0parrot1) parrot-updates; urgency=medium

  * Import new upstream release.

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Tue, 22 Mar 2022 16:44:03 +0100

ruby-cms-scanner (0.13.6-0parrot2) parrot-updates; urgency=medium

  * Fix ruby tests.

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Tue, 22 Mar 2022 15:56:37 +0100

ruby-cms-scanner (0.13.6-0parrot1) parrot-updates; urgency=medium

  * Import new Kali release.

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Tue, 22 Mar 2022 14:53:04 +0100

ruby-cms-scanner (0.13.6-0kali3) kali-dev; urgency=medium

  * Refresh patch for new gems versions packaged in Debian

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 08 Feb 2022 15:10:45 +0100

ruby-cms-scanner (0.13.6-0kali2) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Update email address
  * Consistency with tabs to spaces

  [ Sophie Brun ]
  * Update patch for new ethon in Debian
  * Add missing build-dep

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 27 Dec 2021 21:00:02 +0100

ruby-cms-scanner (0.13.6-0kali1) kali-dev; urgency=medium

  * New upstream version 0.13.6
  * Refresh patches
  * Bump Standards-Version 4.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 23 Sep 2021 12:05:42 +0200

ruby-cms-scanner (0.13.5-0kali1) kali-dev; urgency=medium

  * New upstream version 0.13.5
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Jun 2021 10:22:17 +0200

ruby-cms-scanner (0.13.3-0kali1) kali-dev; urgency=medium

  * New upstream version 0.13.3
  * Refresh patches
  * Update minimal required version of ruby-opt-parse-validator

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Mar 2021 09:00:04 +0100

ruby-cms-scanner (0.13.1-0kali1) kali-dev; urgency=medium

  * New upstream version 0.13.1
  * Fix minimal nokogiri version (see 7023)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Feb 2021 09:48:32 +0100

ruby-cms-scanner (0.13.0-0kali1) kali-dev; urgency=medium

  * New upstream version 0.13.0
  * Refresh patches
  * Bump Standards-Version to 4.5.1 (no changes)
  * debian/watch: use version 4

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 13 Jan 2021 08:45:40 +0100

ruby-cms-scanner (0.12.2-0kali1) kali-dev; urgency=medium

  * New upstream version 0.12.2

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 05 Jan 2021 15:14:41 +0100

ruby-cms-scanner (0.12.1-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sophie Brun ]
  * New upstream version 0.12.1
  * Update minimal required version of opt-parse-validator

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 26 Aug 2020 15:14:55 +0200

ruby-cms-scanner (0.12.0-0kali1) kali-dev; urgency=medium

  * New upstream version 0.12.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Jul 2020 08:46:32 +0200

ruby-cms-scanner (0.10.1-0kali1) kali-dev; urgency=medium

  * New upstream version 0.10.1
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 09 Jun 2020 15:15:00 +0200

ruby-cms-scanner (0.10.0-0kali1) kali-dev; urgency=medium

  * New upstream version 0.10.0
  * Add a patch to fix test of unknown url

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 20 Apr 2020 09:32:31 +0200

ruby-cms-scanner (0.9.0-0kali1) kali-dev; urgency=medium

  * New upstream version 0.9.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Apr 2020 09:47:42 +0200

ruby-cms-scanner (0.8.6-0kali1) kali-dev; urgency=medium

  * New upstream version 0.8.6
  * Update required version
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 16 Mar 2020 18:01:28 +0100

ruby-cms-scanner (0.8.5-0kali2) kali-dev; urgency=medium

  * Update minimal required version of public-suffix

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 12 Mar 2020 13:44:12 +0100

ruby-cms-scanner (0.8.5-0kali1) kali-dev; urgency=medium

  * New upstream version 0.8.5

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 12 Mar 2020 09:37:15 +0100

ruby-cms-scanner (0.8.4-0kali1) kali-dev; urgency=medium

  * New upstream version 0.8.4
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 05 Mar 2020 16:48:11 +0100

ruby-cms-scanner (0.8.1-0kali2) kali-dev; urgency=medium

  * Change minimal required version of typhoeus

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 10 Feb 2020 15:10:54 +0100

ruby-cms-scanner (0.8.1-0kali1) kali-dev; urgency=medium

  * New upstream version 0.8.1

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 03 Jan 2020 10:56:08 +0100

ruby-cms-scanner (0.7.1-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.1
  * Refresh patches
  * Add missing depends ruby-get-process-mem
  * Update minimal version of ruby-opt-parse-validator
  * Bump Standards-Version to 4.4.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 07 Nov 2019 10:03:45 +0100

ruby-cms-scanner (0.6.0-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 16 Sep 2019 14:30:25 +0200

ruby-cms-scanner (0.5.7-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0.5.7
  * Refresh debian/patches
  * Bump Standards-Version to 4.4.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 15 Aug 2019 11:20:54 +0200

ruby-cms-scanner (0.5.4-0kali1) kali-dev; urgency=medium

  * New upstream version 0.5.4

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 12 Jul 2019 11:00:43 +0200

ruby-cms-scanner (0.5.3-0kali1) kali-dev; urgency=medium

  * New upstream version 0.5.3

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 05 Jul 2019 11:33:32 +0200

ruby-cms-scanner (0.5.2-0kali4) kali-dev; urgency=medium

  * debian/rules: export LC_ALL=C.UTF-8 as old sbuild defines LC_ALL=POSIX

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Jul 2019 14:41:08 +0200

ruby-cms-scanner (0.5.2-0kali3) kali-dev; urgency=medium

  * debian/rules: export LANG=C.UTF-8 since the test suite assumes a UTF-8
    locale

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 01 Jul 2019 16:40:18 +0200

ruby-cms-scanner (0.5.2-0kali2) kali-dev; urgency=medium

  * Add missing build-dep and dep procps (for ps command)
  * Use debhelper-compat 12
  * Refresh patch
  * Add patches to run the tests and autopkgtest

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 28 Jun 2019 14:29:53 +0200

ruby-cms-scanner (0.5.2-0kali1) kali-dev; urgency=medium

  * Add debian/gbp.conf
  * New upstream version 0.5.2
  * Refresh patch

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 Jun 2019 14:40:01 +0200

ruby-cms-scanner (0.5.0-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Vcs-* fields for the move to gitlab.com

  [ Sophie Brun ]
  * New upstream version 0.5.0
  * Refresh patch
  * Bump minimal required version of opt-parse-validator

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Apr 2019 12:14:27 +0200

ruby-cms-scanner (0.0.44.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 08 Apr 2019 16:18:05 +0200

ruby-cms-scanner (0.0.43.2-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 05 Apr 2019 11:52:02 +0200

ruby-cms-scanner (0.0.41.4-0kali1) kali-dev; urgency=medium

  * Import new usptream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Mar 2019 16:28:29 +0100

ruby-cms-scanner (0.0.41.3-0kali2) kali-dev; urgency=medium

  * Fix the patch
  * Bump Standards-Version to 4.3.0

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Feb 2019 10:23:36 +0100

ruby-cms-scanner (0.0.41.3-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 14 Jan 2019 16:09:23 +0100

ruby-cms-scanner (0.0.41.1-0kali2) kali-dev; urgency=medium

  * Fix required version for opt-parser

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Jan 2019 14:40:55 +0100

ruby-cms-scanner (0.0.41.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Jan 2019 13:46:33 +0100

ruby-cms-scanner (0.0.41.0-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 14 Nov 2018 15:43:13 +0100

ruby-cms-scanner (0.0.40.3-0kali2) kali-dev; urgency=medium

  * Update patch: use new version of ruby-public-suffix

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 05 Nov 2018 18:54:44 +0100

ruby-cms-scanner (0.0.40.3-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 05 Nov 2018 16:55:06 +0100

ruby-cms-scanner (0.0.40.2-0kali3) kali-dev; urgency=medium

  * Use the latest ruby-opt-parse-validator

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Oct 2018 09:44:12 +0200

ruby-cms-scanner (0.0.40.2-0kali2) kali-dev; urgency=medium

  * Reduce minimal required version of ruby-yajl

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Oct 2018 16:25:28 +0200

ruby-cms-scanner (0.0.40.2-0kali1) kali-dev; urgency=medium

  * Import new usptream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Oct 2018 15:34:58 +0200

ruby-cms-scanner (0.0.40.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 10 Oct 2018 09:16:07 +0200

ruby-cms-scanner (0.0.40-0kali1) kali-dev; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 27 Sep 2018 17:20:55 +0200
